package comptermots

// tableau de caractères qui correspondent à des espaces.
var asciiSpace = [256]uint8{'\t': 1, '\n': 1, '\v': 1, '\f': 1, '\r': 1, ' ': 1}

// Count - Fonction pour compter les mots dans une chaine
func Count(phrase string) int {
	nb := 0
	i := 0
	motStart := 0
	// Suppression des espaces du début
	for i < len(phrase) && asciiSpace[phrase[i]] != 0 {
		i++
	}
	motStart = i
	// Je commence à parcourir le mot
	for i < len(phrase) {
		if asciiSpace[phrase[i]] == 0 {
			i++
			continue
		}
		nb++
		i++
		for i < len(phrase) && asciiSpace[phrase[i]] != 0 {
			i++
		}
		motStart = i
	}
	if motStart < len(phrase) {
		nb++
	}
	return nb
}
